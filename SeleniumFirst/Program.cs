﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeleniumFirst
{
    class Program
    {
        IWebDriver driver = new FirefoxDriver();

        string
            TotalResults,
            Id_InputSearch = "gh-ac",
            Id_ButtonSearch = "gh-btn",
            CssSelector_Brand = "[aria-label*='adidas']",
            CssSelector_Size = "[aria-label='10']",
            CssSelector_Button_Order = "[data-w-onclick*='toggle|w9']",
            CssSelector_OrderASC = "[href*='_sop=15']",
            Class_NumberResults = "srp-controls__count-heading",
            Class_ItemProduct = "s-item__link";

        string[]
            LinkResults = new string[5],
            NamePrices = new string[5],
            ListNames = new string[5];
             
        decimal[]
            ListPrices = new decimal[5];

        static void Main(string[] args)
        {
        }

        [SetUp]
        public void Initialize()
        {
            // Colocamos la URL del sitio que queremos analizar
            driver.Navigate().GoToUrl("https://ebay.com");
        }

        [Test]
        public void ExecuteTest()
        {
            Console.WriteLine("Inicia proceso");

            // Busca el ID del Input de busqueda, y en dicho input se escribe lo que se quiere buscar
            driver.FindElement(By.Id(Id_InputSearch)).SendKeys("Shoes");

            // Busca el ID del botón "Buscar" y hace click a dicho botón
            driver.FindElement(By.Id(Id_ButtonSearch)).Click();

            // Busca en los filtros el botón de la marca Adidas y hace click
            driver.FindElement(By.CssSelector(CssSelector_Brand)).Click();

            // Busca en los filtros el botón de la talla 10 y hace click
            driver.FindElement(By.CssSelector(CssSelector_Size)).Click();

            // Hace click en el botón con las opciones para ordenar las busquedas
            driver.FindElement(By.CssSelector(CssSelector_Button_Order)).Click();

            // Hace click en la opción de busqueda "Precio + Envío: más bajo primero"
            driver.FindElement(By.CssSelector(CssSelector_OrderASC)).Click();

            //Imprime en consola la cantidad de busquedas encontradas
            TotalResults = driver.FindElement(By.ClassName(Class_NumberResults)).Text;
            Console.WriteLine(TotalResults);

            //Entra al detalle de los 5 primeros productos para comenzar a obtener valores para los siguientes pasos
            IList<IWebElement> elements = driver.FindElements(By.ClassName(Class_ItemProduct));

            //Completa el array con el contenido de los primeros 5 resultados
            for (int i = 0; i < 5; i++)
            {
                LinkResults[i] = elements[i].Text;
            }

            //Con cada resultados realiza varias tareas
            for (int i = 0; i < 5; i++)
            {
                if (LinkResults[i] != null)
                {
                    //Entra a la pagina de cada resultado
                    driver.FindElement(By.LinkText(LinkResults[i])).Click();

                    //Guarda en variables los datos del nombre y precio
                    var PriceProduct = driver.FindElement(By.CssSelector("[itemprop*='price']")).GetAttribute("content");
                    var NameProduct = driver.FindElement(By.Id("itemTitle")).Text;

                    // convierte el string del precio a decimal 
                    decimal valdec = Convert.ToDecimal(PriceProduct, new CultureInfo("en-US"));

                    //Guada en un array el nombre más el precio
                    NamePrices[i] = NameProduct + " $" + PriceProduct;

                    //Guada en un array el nombre del producto
                    ListNames[i] = NameProduct;

                    //Guada en un array el precio
                    ListPrices[i] = valdec;

                    //Regresa a la página de los resultados
                    driver.Navigate().Back();
                }
            }

            Console.WriteLine("\r\n ------------------------------- \r\n");
            Console.WriteLine("\r\n Precios y nombres de los 5 primeros productos\r\n");

            // Imprime la lista de los nombres con los precios de los 5 primeros resultados
            PrintMessages(NamePrices);

            Console.WriteLine("\r\n ------------------------------- \r\n");
            Console.WriteLine("\r\n Lista de nombre ordenados de la A-Z \r\n");

            //Ordena los nombres de los productos
            Array.Sort(ListNames);

            // Imprime la lista de los nombre de los 5 primeros resultados
            PrintMessages(ListNames);

            Console.WriteLine("\r\n ------------------------------- \r\n");
            Console.WriteLine("\r\n Lista de precios orenados de Mayor a Menor \r\n");

            //Ordena los precios de mayor a menor
            Array.Sort(ListPrices);

            //Invierte el orden de los precios para quedar de mayor a menor
             Array.Reverse(ListPrices);

            //Imprime en la consola los nombres de los productos ordenados de forma ascendente
            PrintDecimals(ListPrices);
        }

        [TearDown]
        public void CleanUp()
        {
            driver.Close();
        }

        // Función para imprimir los arreglos tipo string 
        static void PrintMessages(string[] ItemString)
        {
            //Imprime en la consola los nombres de los productos con los precios
            foreach (string value in ItemString)
            {
                Console.WriteLine(value);
            }

        }

        // Función para imprimir los números decimales (precios) en orden
        static void PrintDecimals(decimal[] ItemDecimal)
        {
            // Imprime en la consola los precios ya ordenados de mayor a menor
            foreach (decimal value in ItemDecimal)
            {
                Console.WriteLine(value);
            }
        }
    }
}
